# Basic Update Notifier

The Update Notifier will check for updates approximately 2-3 minutes
after computer startup. Thereafter, it will check every 6 hours. If
updates are available, it will show a desktop notification and an icon
in the system tray. The desktop notification shows the number of updates
available and lists them. Hovering your mouse over the system tray icon
will show you the same information.

Run 'Update Notifier Settings' or edit /etc/default/update-notifier
to select the default action when you click on the notification icon or
to change the reporting interval. Reboot to apply the changes.

To change the frequency of updating the package cache, you can edit
/etc/cron.d/updates-sync-cron (See EXAMPLES below.)

If you are using a window manager that doesn't recognize items in
/etc/xdg/autostart/, add the following lines to your autostart file...

    /usr/bin/notifier-first-run.sh &
    /usr/bin/notifier-repeat-run.sh &

# EXAMPLES

## File /etc/cron.d/updates-sync-cron
NOTE: Reboot or restart cron to apply any changes to this file.
For more details, see: man 5 crontab

This will update the cache every six hours, five minutes before
the hour, and on every boot.

    # m h  dom mon dow   command
    55 */6 * * * root /usr/bin/apt-get update
    @reboot root /usr/bin/apt-get update

Once per day at 1:55AM.

    # m h  dom mon dow   command
    55 01 * * * root /usr/bin/apt-get update
    @reboot root /usr/bin/apt-get update

# KNOWN BUGS

When you click on the notification icon in the tray, it will close
any open yad windows in addition to closing the notification icon.
