#!/usr/bin/env sh

DEFAULTS_FILE="/etc/default/update-notifier"

. "${DEFAULTS_FILE}"

ps -eu ${USER} -o pid,command:180 | awk '/[y]ad.*--notification/{print $1}' | xargs kill

if test "$FRONTEND" = terminal ; then
	pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY \
	${TERMINAL_COMMAND} ${TERMINAL_OPTIONS} 'apt-get dist-upgrade'
elif test "$FRONTEND" = gui ; then
	synaptic-pkexec --dist-upgrade-mode --non-interactive
fi

exit 0
